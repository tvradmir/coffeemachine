package com.coffeemachine.pojos;

public class SoldReport {
    private String drink;
    private Long number;
    private double amount;

    public SoldReport(String drink, Long number, double amount) {
        this.drink = drink;
        this.number = number;
        this.amount = amount;
    }

    public String getDrink() {
        return drink;
    }

    public void setDrink(String drink) {
        this.drink = drink;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return  "drink => "+drink + " || number => " + number + " || amount => " + amount+" euro";
    }
}
