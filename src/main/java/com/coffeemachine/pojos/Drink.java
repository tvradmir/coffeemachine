package com.coffeemachine.pojos;

public class Drink {

    private Integer suger;
    private boolean stick;
    private TypeDrink type;

    public Drink(Integer suger, TypeDrink type) {
        this.suger = suger;
        this.type = type;
        if(suger!=null && suger != 0){
            this.stick = true;
        }
    }

    public Integer getSuger() {
        return suger;
    }


    public boolean isStick() {
        return stick;
    }

    public TypeDrink getType() {
        return type;
    }

}
