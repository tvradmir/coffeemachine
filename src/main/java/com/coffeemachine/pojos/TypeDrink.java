package com.coffeemachine.pojos;

public enum TypeDrink {
    TEA("Th",0.4),
    CHOCOLATE("Hh",0.5),
    COFFEE("Ch",0.6),
    ORANGE("O",0.6);

    TypeDrink(String label, double prix) {
        this.label = label;
        this.price = prix;
    }

    private String label;
    private double price;

    public String getLabel() {
        return label;
    }

    public double getPrice() {
        return price;
    }
}
