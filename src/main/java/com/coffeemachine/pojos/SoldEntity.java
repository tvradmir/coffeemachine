package com.coffeemachine.pojos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class SoldEntity {
    @Id
    @GeneratedValue
    private int id;
    @Column
    private String drink;
    @Column
    private double realPrice;
    @Column
    private double givenPrice;
    @Column
    private Date date;

    public SoldEntity() {
    }

    public SoldEntity(String drink, double realPrice, double givenPrice) {
        this.drink = drink;
        this.realPrice = realPrice;
        this.givenPrice = givenPrice;
        this.date= new Date();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDrink() {
        return drink;
    }

    public void setDrink(String drink) {
        this.drink = drink;
    }

    public double getRealPrice() {
        return realPrice;
    }

    public void setRealPrice(double price) {
        this.realPrice = price;
    }

    public double getGivenPrice() {
        return givenPrice;
    }

    public void setGivenPrice(double change) {
        this.givenPrice = change;
    }

}
