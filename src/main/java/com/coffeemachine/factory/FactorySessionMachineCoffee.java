package com.coffeemachine.factory;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class FactorySessionMachineCoffee {

    public static SessionFactory createSessionFactory() {
        return new Configuration().configure().buildSessionFactory();
    }

    public void closeFactory(SessionFactory sessionFactory){
        sessionFactory.close();
    }
}
