package com.coffeemachine.exception;

public class MachineCoffeeException extends  Exception {


    public MachineCoffeeException() {
        super();
    }
    public MachineCoffeeException(String s) {
        super(s);
    }

    public MachineCoffeeException(String message, Throwable cause) {
        super(message, cause);
    }

}
