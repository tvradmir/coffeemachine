package com.coffeemachine.dao;
import org.hibernate.Session;

import java.util.List;

public interface IDao<T> {
    void save(Session session, T t);
    List<T> getAll(Session session);
    void saveAll(Session sessin, List<T> list);
    void creanUp(Session session);

}
