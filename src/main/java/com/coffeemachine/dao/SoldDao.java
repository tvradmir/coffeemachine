package com.coffeemachine.dao;
import com.coffeemachine.pojos.SoldEntity;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;


import java.util.ArrayList;
import java.util.List;

public class SoldDao implements IDao<SoldEntity> {

    @Override
    public void save(Session session, SoldEntity reportEntity) {
        session.beginTransaction();
        session.save(reportEntity);
        session.getTransaction().commit();
    }

    @Override
    public List<SoldEntity> getAll(Session session) {
        return session.createQuery("FROM SoldEntity").list();
    }

    @Override
    public void saveAll(Session session, List<SoldEntity> list) {
        List<SoldEntity> result = new ArrayList<>();
        list.forEach(soldEntity -> {
            save(session,soldEntity);
            result.add(soldEntity);
        });
    }



    @Override
    public  void creanUp(Session session){
        Transaction transaction = session.getTransaction();
        transaction.begin();
        String stringQuery = "DELETE FROM SoldEntity";
        Query query = session.createQuery(stringQuery);
        query.executeUpdate();
        transaction.commit();
    }
}
