package com.coffeemachine.services;

import com.coffeemachine.exception.MachineCoffeeException;
import com.coffeemachine.pojos.Drink;
import com.coffeemachine.pojos.TypeDrink;
import org.apache.commons.lang.StringUtils;

public class GeneratorDrinkMakerProtocolImpl implements IGaneratorDrinkMakerProtocol {

    @Override
    public String generateOrderDrinkProtocol(Drink drink) throws MachineCoffeeException {
        throwingExceptionForDrinkMakerProtocol(drink);

        return   appendSugerOnProtocol(drink);
    }

    private void throwingExceptionForDrinkMakerProtocol(Drink drink) throws MachineCoffeeException {
        if(drink == null){
            throw new MachineCoffeeException("Drink can not be null");
        }
        if(drink.getType() == null){
            throw new MachineCoffeeException("Type of drink can not be null");
        }
        if(drink.getType() == TypeDrink.ORANGE && (drink.getSuger()!=null && drink.getSuger() >0)){
            throw new MachineCoffeeException("Suger can not be added in orange juice");
        }
    }

    private String appendSugerOnProtocol(Drink drink) {
        StringBuilder drinkMakerProtocolBuilder = new StringBuilder(drink.getType().getLabel());
        if (drink.getSuger()!=null && drink.getSuger()!=0){
            drinkMakerProtocolBuilder.append(":"+drink.getSuger()).append(":0");
        }else{
            drinkMakerProtocolBuilder.append(": : ");
        }
        return drinkMakerProtocolBuilder.toString();
    }

    @Override
    public String generateOrderMessageProtocol(String messageContent) throws MachineCoffeeException {

        if(StringUtils.isBlank(messageContent)){
            throw new MachineCoffeeException("Message can not be empty or null");
        }
        return "M:"+messageContent;
    }
}
