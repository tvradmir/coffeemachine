package com.coffeemachine.services;

import com.coffeemachine.exception.MachineCoffeeException;
import com.coffeemachine.pojos.Drink;

public interface IGaneratorDrinkMakerProtocol {

    public String generateOrderDrinkProtocol(Drink drink)  throws MachineCoffeeException;
    public String generateOrderMessageProtocol(String message)  throws MachineCoffeeException;
}
