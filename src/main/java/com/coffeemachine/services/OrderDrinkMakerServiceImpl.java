package com.coffeemachine.services;

import com.coffeemachine.exception.MachineCoffeeException;
import com.coffeemachine.pojos.Drink;
import com.coffeemachine.pojos.Order;
import com.coffeemachine.pojos.SoldEntity;
import com.coffeemachine.pojos.TypeDrink;

public class OrderDrinkMakerServiceImpl implements IOrderDrinkMaker {

    private IGaneratorDrinkMakerProtocol ganeratorDrinkMakerProtocol;
    private ISoldService soldService;
    private BeverageQuantityChecker beverageQuantityChecker;
    private EmailNotifier emailNotifier;

    public OrderDrinkMakerServiceImpl(IGaneratorDrinkMakerProtocol ganeratorDrinkMakerProtocol, ISoldService soldService, BeverageQuantityChecker beverageQuantityChecker, EmailNotifier emailNotifier) {
        this.ganeratorDrinkMakerProtocol = ganeratorDrinkMakerProtocol;
        this.soldService = soldService;
        this.beverageQuantityChecker = beverageQuantityChecker;
        this.emailNotifier = emailNotifier;
    }

    public OrderDrinkMakerServiceImpl(IGaneratorDrinkMakerProtocol ganeratorDrinkMakerProtocol, ISoldService soldService) {
        this.ganeratorDrinkMakerProtocol = ganeratorDrinkMakerProtocol;
        this.soldService = soldService;
    }

    @Override
    public String orderDrink(Order order) throws MachineCoffeeException {
        String orderCommand = null;
        if (order != null) {
            Drink drink = order.getDrink();
            if (drink != null) {
                TypeDrink type = drink.getType();
                double priceOfDrink = type.getPrice();
                double givenPrice = order.getPrice();
                if (givenPrice < 0) {
                    throw new MachineCoffeeException("Price can not be negative");
                }
                if (givenPrice >= priceOfDrink) {
                    String label = type.getLabel();
                    if (beverageQuantityChecker.isEmpty(label)) {
                        emailNotifier.notifyMissingDrink(label);
                        String contentMessage = "Shortages of Water, the notification has been sent to the company";
                        orderCommand = this.ganeratorDrinkMakerProtocol.generateOrderMessageProtocol(contentMessage);
                    } else {
                        orderCommand = this.ganeratorDrinkMakerProtocol.generateOrderDrinkProtocol(drink);
                        SoldEntity soldEntity = new SoldEntity(type.name(), type.getPrice(), givenPrice);
                        soldService.save(soldEntity);
                    }

                } else {
                    double missingAmount = getMissingAmount(priceOfDrink, givenPrice);
                    String contentMessage = "missing " + missingAmount + " euro";
                    orderCommand = this.ganeratorDrinkMakerProtocol.generateOrderMessageProtocol(contentMessage);
                }
            }
        }
        return orderCommand;
    }


    public BeverageQuantityChecker getBeverageQuantityChecker() {
        return beverageQuantityChecker;
    }

    public void setBeverageQuantityChecker(BeverageQuantityChecker beverageQuantityChecker) {
        this.beverageQuantityChecker = beverageQuantityChecker;
    }

    public EmailNotifier getEmailNotifier() {
        return emailNotifier;
    }

    public void setEmailNotifier(EmailNotifier emailNotifier) {
        this.emailNotifier = emailNotifier;
    }

    private double getMissingAmount(double priceOfDrink, double givenPrice) {
        return priceOfDrink - givenPrice;
    }

}
