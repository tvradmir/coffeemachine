package com.coffeemachine.services;

import com.coffeemachine.pojos.SoldEntity;

import java.util.List;

public interface ISoldService {
    void save( SoldEntity t);
    List<SoldEntity> getAll();
    void saveAll(List<SoldEntity> list);
    void creanUp();



}
