package com.coffeemachine.services;

public interface BeverageQuantityChecker {
    boolean isEmpty(String drink);
}
