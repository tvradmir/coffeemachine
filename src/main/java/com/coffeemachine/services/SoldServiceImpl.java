package com.coffeemachine.services;

import com.coffeemachine.dao.IDao;
import com.coffeemachine.dao.SoldDao;
import com.coffeemachine.factory.FactorySessionMachineCoffee;
import com.coffeemachine.pojos.SoldEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;


import java.util.List;

public class SoldServiceImpl implements ISoldService {

    private SessionFactory sessionFactory;

    private IDao<SoldEntity> reportEntityIDao;

    public SoldServiceImpl() {
        this.reportEntityIDao = new SoldDao();
    }

    @Override
    public void save(SoldEntity soldtEntity) {
        sessionFactory = FactorySessionMachineCoffee.createSessionFactory();
        Session session = sessionFactory.openSession();
        try {
            reportEntityIDao.save(session,soldtEntity);
        }finally {
            session.close();
            sessionFactory.close();
        }
    }

    @Override
    public List<SoldEntity> getAll() {
        sessionFactory = FactorySessionMachineCoffee.createSessionFactory();
        Session session = sessionFactory.openSession();
        List<SoldEntity> result = null;
        try {
            result = reportEntityIDao.getAll(session);
        }finally {
            session.close();
            sessionFactory.close();
        }
        return result;
    }

    @Override
    public void saveAll(List<SoldEntity> list) {
        List<SoldEntity> result = null;
        sessionFactory = FactorySessionMachineCoffee.createSessionFactory();
        Session session = sessionFactory.openSession();
        try {
            reportEntityIDao.saveAll(session,list);
        }finally {
            session.close();
            sessionFactory.close();
        }
    }

    @Override
    public void creanUp() {
        sessionFactory = FactorySessionMachineCoffee.createSessionFactory();
        Session session = sessionFactory.openSession();
        try {
            reportEntityIDao.creanUp(session);
        }finally {
            session.close();
            sessionFactory.close();
        }

    }


}
