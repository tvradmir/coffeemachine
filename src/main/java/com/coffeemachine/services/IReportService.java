package com.coffeemachine.services;


import com.coffeemachine.pojos.SoldReport;

import java.util.List;

public interface IReportService {

    List<SoldReport> getReport();
}
