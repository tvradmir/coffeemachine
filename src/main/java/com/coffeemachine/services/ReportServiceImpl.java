package com.coffeemachine.services;

import com.coffeemachine.pojos.SoldEntity;
import com.coffeemachine.pojos.SoldReport;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ReportServiceImpl implements IReportService {

    private ISoldService soldService;

    public ReportServiceImpl(ISoldService soldService) {
        this.soldService = soldService;
    }

    @Override
    public List<SoldReport> getReport(){
        List<SoldReport>  soldReports = new ArrayList<>();
        List<SoldEntity> soldEntityList = soldService.getAll();
        Map<String, Long> collectDrinkCount = soldEntityList.stream().collect(Collectors.groupingBy(SoldEntity::getDrink, Collectors.counting()));
        Map<String, Double> collectDrinkAmount = soldEntityList.stream().collect(Collectors.groupingBy(SoldEntity::getDrink, Collectors.summingDouble(SoldEntity::getRealPrice)));
       collectDrinkCount.forEach((k,v)->soldReports.add( new SoldReport(k,v,collectDrinkAmount.get(k))));
        printReport(soldReports);

        return soldReports;

    }

    private void printReport(List<SoldReport> soldReports) {
        System.out.println("REPORT");
        if (soldReports.isEmpty()){
          System.out.println("Not Drink Sold");
        }else{
            soldReports.forEach(soldReport -> System.out.println(soldReport.toString()));
            double  totalAmount = soldReports.stream().mapToDouble(SoldReport::getAmount).sum();
            System.out.println("TOTAL: "+totalAmount+" euros");

        }
    }
}
