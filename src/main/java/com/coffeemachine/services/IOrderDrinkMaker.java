package com.coffeemachine.services;

import com.coffeemachine.exception.MachineCoffeeException;
import com.coffeemachine.pojos.Order;

public interface IOrderDrinkMaker {
    public String orderDrink(Order order)  throws MachineCoffeeException;
}
