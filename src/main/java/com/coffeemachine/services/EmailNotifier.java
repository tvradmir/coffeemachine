package com.coffeemachine.services;

public interface EmailNotifier {
    void notifyMissingDrink(String drink);
}
