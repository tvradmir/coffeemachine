package com.coffeemachine.services;

import com.coffeemachine.exception.MachineCoffeeException;
import com.coffeemachine.pojos.Drink;
import com.coffeemachine.pojos.TypeDrink;
import org.hamcrest.CoreMatchers;
import org.junit.*;
import org.junit.rules.ExpectedException;


public class GeneratorDrinkMakerProtocolServiceTest {


    private static IGaneratorDrinkMakerProtocol generatorDrinkMakerProtocolService;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @BeforeClass
    public  static void setUpGeneratorDrinkMaker() {
        generatorDrinkMakerProtocolService = new GeneratorDrinkMakerProtocolImpl();
    }


    @Test
    public void shouldThrowMachineCoffeeExceptionWhenGenerateOrderDrinkProtocolGivenDrinkNull() throws MachineCoffeeException {

        //Given
        Drink nullDrink =null;

        thrown.expect(MachineCoffeeException.class);
        thrown.expectMessage(CoreMatchers.containsString("Drink can not be null"));

        //When
        generatorDrinkMakerProtocolService.generateOrderDrinkProtocol(nullDrink);


    }

    @Test
    public void shoulThrowMachineCoffeeExceptionWhenGenerateOrderDrinkProtocolGivenDrinkWhithTypeDrinkNull() throws MachineCoffeeException {

        //Given
        Drink emptyDrink = new Drink(0,null);

        thrown.expect(MachineCoffeeException.class);
        thrown.expectMessage(CoreMatchers.containsString("Type of drink can not be null"));

        //When
        generatorDrinkMakerProtocolService.generateOrderDrinkProtocol(emptyDrink);

    }


    @Test
    public void shoulReturnDrinkMakerProtocolOfTeaWhithNonSugerAndNoStickWhenOrderDrinkGivenDrinkWhithTypeDrinkTeaAndnSugerNull() throws MachineCoffeeException {
        //Given
        TypeDrink tea = TypeDrink.TEA;
        Drink TeaWithNoSuger = new Drink(null, tea);
        //When
        String actualOrder = generatorDrinkMakerProtocolService.generateOrderDrinkProtocol(TeaWithNoSuger);

        //Then
        Assert.assertEquals(tea.getLabel()+": : ", actualOrder);

    }


    @Test
    public void shoulReturnDrinkMakerProtocolOfTeaWhithZeroSugerAndNoStickWhenOrderDrinkGivenDrinkWhithTypeDrinkTeaAndZeroSuger() throws MachineCoffeeException {
        //Given
        TypeDrink tea = TypeDrink.TEA;
        Drink TeaWithNoSuger = new Drink(0, tea);
        //When
        String actualOrder = generatorDrinkMakerProtocolService.generateOrderDrinkProtocol(TeaWithNoSuger);

        //Then
        Assert.assertEquals(tea.getLabel()+": : ", actualOrder);

    }

    @Test
    public void shoulReturnDrinkMakerProtocolOfTeaWhithOneSugerAndNoStickWhenOrderDrinkGivenDrinkWhithTypeDrinkTeaAndOneSuger() throws MachineCoffeeException {
        //Given
        TypeDrink tea = TypeDrink.TEA;
        Drink TeaWithNoSuger = new Drink(1, tea);
        //When
        String actualOrder = generatorDrinkMakerProtocolService.generateOrderDrinkProtocol(TeaWithNoSuger);

        //Then
        Assert.assertEquals(tea.getLabel()+":1:0", actualOrder);

    }

    @Test
    public void shoulReturnDrinkMakerProtocolOfTeaWhithTwoSugerAndNoStickWhenOrderDrinkGivenDrinkWhithTypeDrinkTeaAndTwoSuger() throws MachineCoffeeException {
        //Given
        TypeDrink tea = TypeDrink.TEA;
        Drink TeaWithNoSuger = new Drink(2, tea);
        //When
        String actualOrder = generatorDrinkMakerProtocolService.generateOrderDrinkProtocol(TeaWithNoSuger);

        //Then
        Assert.assertEquals(tea.getLabel()+":2:0", actualOrder);

    }



    @Test
    public void shoulReturnDrinkMakerProtocolOfChocolateWithNonSugerAndNoStickWhenOrderDrinkGivenDrinkWithTypeDrinkChocolatAndAndSugeNnull() throws MachineCoffeeException {
        //Given
        TypeDrink chocolate = TypeDrink.CHOCOLATE;
        Drink TeaWithNoSuger = new Drink(null, chocolate);
        //When
        String actualOrder = generatorDrinkMakerProtocolService.generateOrderDrinkProtocol(TeaWithNoSuger);

        //Then
        Assert.assertEquals(chocolate.getLabel()+": : ", actualOrder);

    }

    @Test
    public void shoulReturnDrinkMakerProtocolOfChocolateWithZeroSugerAndNoStickWhenOrderDrinkGivenDrinkWithTypeDrinkChocolatAndNoSuger() throws MachineCoffeeException {
        //Given
        TypeDrink chocolate = TypeDrink.CHOCOLATE;
        Drink TeaWithNoSuger = new Drink(0, chocolate);
        //When
        String actualOrder = generatorDrinkMakerProtocolService.generateOrderDrinkProtocol(TeaWithNoSuger);

        //Then
        Assert.assertEquals(chocolate.getLabel()+": : ", actualOrder);

    }

    @Test
    public void shoulReturnDrinkMakerProtocolOfChocolateWithOneSugerAndOneStickWhenOderDrinkGivenDrinkWithTypeDrinkChocolatAndOneSuger() throws MachineCoffeeException {
        //Given
        TypeDrink chocolate = TypeDrink.CHOCOLATE;
        Drink TeaWithNoSuger = new Drink(1, chocolate);
        //When
        String actualOrder = generatorDrinkMakerProtocolService.generateOrderDrinkProtocol(TeaWithNoSuger);

        //Then
        Assert.assertEquals(chocolate.getLabel()+":1:0", actualOrder);

    }

    @Test
    public void shoulReturnDrinkMakerProtocolOfChocolatWhithTwoSugerAndNoStickWhenOrderDrinkGivenDrinkWhithTypeDrinkChocolatAndTwoSuger() throws MachineCoffeeException {
        //Given
        TypeDrink chocolate = TypeDrink.CHOCOLATE;
        Drink TeaWithNoSuger = new Drink(2, chocolate);
        //When
        String actualOrder = generatorDrinkMakerProtocolService.generateOrderDrinkProtocol(TeaWithNoSuger);

        //Then
        Assert.assertEquals(chocolate.getLabel()+":2:0", actualOrder);

    }


    @Test
    public void shoulReturnDrinkMakerProtocolOfCoffeeWithNonSugerAndNoStickWhenOrderDrinkGivenDrinkWithTypeDrinkCoffeeAndSugerNull() throws MachineCoffeeException {
        //Given
        TypeDrink coffee = TypeDrink.COFFEE;
        Drink TeaWithNoSuger = new Drink(null, coffee);
        //When
        String actualOrder = generatorDrinkMakerProtocolService.generateOrderDrinkProtocol(TeaWithNoSuger);

        //Then
        Assert.assertEquals(coffee.getLabel()+": : ", actualOrder);

    }

    @Test
    public void shoulReturnDrinkMakerProtocolOfCoffeeWithZeroSugerAndNoStickWhenOrderDrinkGivenDrinkWithTypeDrinkCoffeeAndNoSuger() throws MachineCoffeeException {
        //Given
        TypeDrink coffee = TypeDrink.COFFEE;
        Drink TeaWithNoSuger = new Drink(0, coffee);
        //When
        String actualOrder = generatorDrinkMakerProtocolService.generateOrderDrinkProtocol(TeaWithNoSuger);

        //Then
        Assert.assertEquals(coffee.getLabel()+": : ", actualOrder);

    }

    @Test
    public void shoulReturnDrinkMakerProtocolOfCoffeeWithOneSugerAndOneStickWhenOrderDrinkGivenDrinkWithTypeDrinkCoffeeAndOneSuger() throws MachineCoffeeException {
        //Given
        TypeDrink coffee = TypeDrink.COFFEE;
        Drink TeaWithNoSuger = new Drink(1, coffee);
        //When
        String actualOrder = generatorDrinkMakerProtocolService.generateOrderDrinkProtocol(TeaWithNoSuger);

        //Then
        Assert.assertEquals(coffee.getLabel()+":1:0", actualOrder);

    }

    @Test
    public void shoulReturnDrinkMakerProtocolOfCoffeeWithTwoSugerAndOneStickWhenOrderDrinkGivenDrinkWithTypeDrinkCoffeeAndTwoSuger() throws MachineCoffeeException {
        //Given
        TypeDrink coffee = TypeDrink.COFFEE;
        Drink TeaWithNoSuger = new Drink(2, coffee);
        //When
        String actualOrder = generatorDrinkMakerProtocolService.generateOrderDrinkProtocol(TeaWithNoSuger);

        //Then
        Assert.assertEquals(coffee.getLabel()+":2:0", actualOrder);

    }

    @Test
    public void shoulThrowMachineCoffeeExceptionWhenOrderMessageGivenMessageMull() throws MachineCoffeeException {

        //Given
        String message =null;

        thrown.expect(MachineCoffeeException.class);
        thrown.expectMessage(CoreMatchers.containsString("Message can not be empty or null"));

        //When
        generatorDrinkMakerProtocolService.generateOrderMessageProtocol(message);


    }

    @Test
    public void shoulThrowMachineCoffeeExceptionwhenOrderMessageGivenEmpty() throws MachineCoffeeException {

        //Given
        String message ="";

        thrown.expect(MachineCoffeeException.class);
        thrown.expectMessage(CoreMatchers.containsString("Message can not be empty or null"));

        //When
        generatorDrinkMakerProtocolService.generateOrderMessageProtocol(message);


    }

    @Test
    public void shoulThrowOrderExceptionWhenOrderMessageGivenBlankMessage() throws MachineCoffeeException {

        //Given
        String message ="   ";

        thrown.expect(MachineCoffeeException.class);
        thrown.expectMessage(CoreMatchers.containsString("Message can not be empty or null"));

        //When
        generatorDrinkMakerProtocolService.generateOrderMessageProtocol(message);


    }



    @Test
    public void shoulReturnDrinkMakerProtocolWithMassageWhenOrderMessageGivenDrinkWithTypeDrinkChocolatAndOneSuger() throws MachineCoffeeException {
        //Given
        String meassage = "Hello! this is the test message";
        //When
        String actualOrder = generatorDrinkMakerProtocolService.generateOrderMessageProtocol(meassage);

        //Then
        Assert.assertEquals("M:"+meassage, actualOrder);

    }


    @Test
    public void shoulReturnDrinkMakerProtocolOfOrangeJuiceWithWhenOrderDrinkGivenDrinkWhithTypeDrinkOrangeAndnSugerNull() throws MachineCoffeeException {
        //Given
        TypeDrink orangeType = TypeDrink.ORANGE;
        Drink orange = new Drink(null, orangeType);
        //When
        String actualOrder = generatorDrinkMakerProtocolService.generateOrderDrinkProtocol(orange);

        //Then
        Assert.assertEquals(orangeType.getLabel()+": : ", actualOrder);

    }


    @Test
    public void shoulReturnDrinkMakerProtocolOfOrangeJuiceWithZeroSugerAndNoStickWhenOrderDrinkGivenDrinkWhithTypeDrinkTeaAndZeroSuger() throws MachineCoffeeException {
        //Given
        TypeDrink orangeType = TypeDrink.ORANGE;
        Drink orange = new Drink(0, orangeType);
        //When
        String actualOrder = generatorDrinkMakerProtocolService.generateOrderDrinkProtocol(orange);

        //Then
        Assert.assertEquals(orangeType.getLabel()+": : ", actualOrder);

    }


    @Test
    public void shoulThrowMachineCoffeeExceptionWhenGenerateOrderDrinkProtocolGivenDrinkWhithTypeDrinkOrangeAndOneSuger() throws MachineCoffeeException {

        //Given
        TypeDrink orangeType = TypeDrink.ORANGE;
        Drink orangeDrink = new Drink(1,orangeType);

        thrown.expect(MachineCoffeeException.class);
        thrown.expectMessage(CoreMatchers.containsString("Suger can not be added in orange juice"));

        //When
        generatorDrinkMakerProtocolService.generateOrderDrinkProtocol(orangeDrink);

    }




}
