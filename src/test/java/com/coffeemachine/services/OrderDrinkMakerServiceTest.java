package com.coffeemachine.services;

import com.coffeemachine.exception.MachineCoffeeException;
import com.coffeemachine.pojos.*;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.*;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class OrderDrinkMakerServiceTest {


    @InjectMocks
    private OrderDrinkMakerServiceImpl orderDrinkMakerService;

    @Mock
    private IGaneratorDrinkMakerProtocol generatorDrinkMakerProtocol;

    @Mock
    private ISoldService soldService;

    @Mock
    private BeverageQuantityChecker beverageQuantityChecker;

    @Mock
    private EmailNotifier emailNotifier;

    @Captor
    private ArgumentCaptor<Drink> drinkCaptor;

    @Captor
    private ArgumentCaptor<String> stringCaptor;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldCallGenerateOrderMessageProtocolAndNotGenerateOrderDrinkProtocolWhenOrderDrnkWithNotenoughMoney() throws MachineCoffeeException {
        //Given
        Order order = new Order();
        Drink drink = new Drink(0, TypeDrink.CHOCOLATE);
        order.setDrink(drink);
        order.setPrice(0.0);

        //When
        orderDrinkMakerService.orderDrink(order);

        //Then
        verify(generatorDrinkMakerProtocol).generateOrderMessageProtocol(stringCaptor.capture());
        //And
        verify(generatorDrinkMakerProtocol,never()).generateOrderDrinkProtocol(drinkCaptor.capture());

    }

    @Test
    public void shouldRetournMessageContainsAmountofMissingMoneyWhenOrderDrnkWithNotenoughMoney() throws MachineCoffeeException {
        //Given
        Order order = new Order();
        Drink drink = new Drink(0, TypeDrink.CHOCOLATE);
        order.setDrink(drink);
        order.setPrice(0.0);
        String missingAmount = String.valueOf(drink.getType().getPrice() - order.getPrice());
        when(generatorDrinkMakerProtocol.generateOrderMessageProtocol(anyString())).thenReturn( "missing "+ missingAmount +" euro");

        //When
        String actualMessage = orderDrinkMakerService.orderDrink(order);

        //Then
        assertTrue(actualMessage.contains(missingAmount));

        verify(generatorDrinkMakerProtocol).generateOrderMessageProtocol(stringCaptor.capture());
        verify(generatorDrinkMakerProtocol,never()).generateOrderDrinkProtocol(drinkCaptor.capture());

    }


    @Test
    public void shouldCallGenerateOrderDrinkProtocolAndNotGenerateOrderMessageProtocolWhenOrderDrnkWithGivenMoneyEqualsToRequiredMoney() throws MachineCoffeeException {
        //Given
        Order order = new Order();
        Drink drink = new Drink(0, TypeDrink.CHOCOLATE);
        order.setDrink(drink);
        order.setPrice(0.5);
        doNothing().when(soldService).save(any(SoldEntity.class));
        when(beverageQuantityChecker.isEmpty(anyString())).thenReturn(false);
        doNothing().when(emailNotifier).notifyMissingDrink(anyString());

        //When
        orderDrinkMakerService.orderDrink(order);
        //Then
        verify(generatorDrinkMakerProtocol,never()).generateOrderMessageProtocol(stringCaptor.capture());
        verify(beverageQuantityChecker).isEmpty(stringCaptor.capture());
        verify(emailNotifier,never()).notifyMissingDrink(stringCaptor.capture());

        //And
        verify(generatorDrinkMakerProtocol).generateOrderDrinkProtocol(drinkCaptor.capture());
    }

    @Test
    public void shouldCallGenerateOrderDrinkProtocolAndNotGenerateOrderMessageProtocolWhenOrderDrnkWithGivenMoneyGreaterToRequiredMoney() throws MachineCoffeeException {
        //Given
        Order order = new Order();
        Drink drink = new Drink(0, TypeDrink.CHOCOLATE);
        order.setDrink(drink);
        order.setPrice(1.0);
        doNothing().when(soldService).save(any(SoldEntity.class));
        when(beverageQuantityChecker.isEmpty(anyString())).thenReturn(false);
        doNothing().when(emailNotifier).notifyMissingDrink(anyString());


        //When
        orderDrinkMakerService.orderDrink(order);
        //Then
        verify(generatorDrinkMakerProtocol,never()).generateOrderMessageProtocol(stringCaptor.capture());
        verify(beverageQuantityChecker).isEmpty(stringCaptor.capture());
        verify(emailNotifier,never()).notifyMissingDrink(stringCaptor.capture());

        //And
        verify(generatorDrinkMakerProtocol).generateOrderDrinkProtocol(drinkCaptor.capture());
    }


    @Test
    public void shouldRetournDrinkMakerProtocolWhenOrderDrnkWithGivenMoneyEqualsToRequiredMoney() throws MachineCoffeeException {
        //Given
        Order order = new Order();
        Drink drink = new Drink(0, TypeDrink.CHOCOLATE);
        order.setDrink(drink);
        order.setPrice(0.5);
        String typeDrinkKabel = drink.getType().getLabel();
        String protocol = typeDrinkKabel +": : ";
        when(generatorDrinkMakerProtocol.generateOrderDrinkProtocol(any(Drink.class))).thenReturn(protocol);
        doNothing().when(soldService).save(any(SoldEntity.class));

        when(beverageQuantityChecker.isEmpty(anyString())).thenReturn(false);
        doNothing().when(emailNotifier).notifyMissingDrink(anyString());

        //When
        String actualMessage = orderDrinkMakerService.orderDrink(order);
        //Then
        assertTrue(actualMessage.contains(typeDrinkKabel));
        verify(generatorDrinkMakerProtocol,never()).generateOrderMessageProtocol(stringCaptor.capture());
        verify(generatorDrinkMakerProtocol).generateOrderDrinkProtocol(drinkCaptor.capture());
        verify(beverageQuantityChecker).isEmpty(stringCaptor.capture());
        verify(emailNotifier,never()).notifyMissingDrink(stringCaptor.capture());

    }


    @Test
    public void shouldRetournDrinkMakerProtocolWhenOrderDrnkWithGivenMoneyGreaterToRequiredMoney() throws MachineCoffeeException {
        //Given
        Order order = new Order();
        Drink drink = new Drink(0, TypeDrink.CHOCOLATE);
        order.setDrink(drink);
        order.setPrice(0.6);
        String typeDrinkKabel = drink.getType().getLabel();
        String protocol = typeDrinkKabel +": : ";
        when(generatorDrinkMakerProtocol.generateOrderDrinkProtocol(any(Drink.class))).thenReturn(protocol);
        doNothing().when(soldService).save(any(SoldEntity.class));

        when(beverageQuantityChecker.isEmpty(anyString())).thenReturn(false);
        doNothing().when(emailNotifier).notifyMissingDrink(anyString());


        //When
        String actualMessage = orderDrinkMakerService.orderDrink(order);
        //Then
        assertTrue(actualMessage.contains(typeDrinkKabel));
        verify(generatorDrinkMakerProtocol,never()).generateOrderMessageProtocol(stringCaptor.capture());
        verify(generatorDrinkMakerProtocol).generateOrderDrinkProtocol(drinkCaptor.capture());
        verify(beverageQuantityChecker).isEmpty(stringCaptor.capture());
        verify(emailNotifier,never()).notifyMissingDrink(stringCaptor.capture());


    }

    @Test
    public void shoulThrowMachineCoffeeExceptionWhenGenerateOrderDrinkProtocolGivenDrinkWhithOrderWithNeagativeAmount() throws MachineCoffeeException {

        Order order = new Order();
        Drink drink = new Drink(0, TypeDrink.CHOCOLATE);
        order.setDrink(drink);
        order.setPrice(-0.5);

        thrown.expect(MachineCoffeeException.class);
        thrown.expectMessage(CoreMatchers.containsString("Price can not be negative"));

        //When
        orderDrinkMakerService.orderDrink(order);

    }



    @Test
    public void shouldCallEmailNotificationWhenOrderDrnkWithGivenShortageOfWaterOrMilk() throws MachineCoffeeException {
        //Given
        Order order = new Order();
        Drink drink = new Drink(0, TypeDrink.CHOCOLATE);
        order.setDrink(drink);
        order.setPrice(0.5);
        String protocol = "Shortages of Water, the notification has been sent to the company";
        when(generatorDrinkMakerProtocol.generateOrderMessageProtocol(anyString())).thenReturn(protocol);
        doNothing().when(soldService).save(any(SoldEntity.class));

        when(beverageQuantityChecker.isEmpty(anyString())).thenReturn(true);
        doNothing().when(emailNotifier).notifyMissingDrink(anyString());

        //When
        orderDrinkMakerService.orderDrink(order);
        //Then
        verify(generatorDrinkMakerProtocol).generateOrderMessageProtocol(stringCaptor.capture());
        verify(generatorDrinkMakerProtocol,never()).generateOrderDrinkProtocol(drinkCaptor.capture());
        verify(beverageQuantityChecker).isEmpty(stringCaptor.capture());
        verify(emailNotifier).notifyMissingDrink(stringCaptor.capture());

    }


    @Test
    public void shouldCallGenerateOrderDrinkProtocolWhenOrderDrnkWithGivenShortageOfWaterOrMilk() throws MachineCoffeeException {
        //Given
        Order order = new Order();
        Drink drink = new Drink(0, TypeDrink.CHOCOLATE);
        order.setDrink(drink);
        order.setPrice(0.5);
        String protocol = "Shortages of Water, the notification has been sent to the company";
        when(generatorDrinkMakerProtocol.generateOrderMessageProtocol(anyString())).thenReturn(protocol);
        doNothing().when(soldService).save(any(SoldEntity.class));

        when(beverageQuantityChecker.isEmpty(anyString())).thenReturn(true);
        doNothing().when(emailNotifier).notifyMissingDrink(anyString());

        //When
        String actualMessage = orderDrinkMakerService.orderDrink(order);
        //Then
        assertTrue(actualMessage.equals(protocol));
        verify(generatorDrinkMakerProtocol).generateOrderMessageProtocol(stringCaptor.capture());
        verify(generatorDrinkMakerProtocol,never()).generateOrderDrinkProtocol(drinkCaptor.capture());
        verify(beverageQuantityChecker).isEmpty(stringCaptor.capture());
        verify(emailNotifier).notifyMissingDrink(stringCaptor.capture());

    }





}
