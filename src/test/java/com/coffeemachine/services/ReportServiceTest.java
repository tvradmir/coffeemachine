package com.coffeemachine.services;

import com.coffeemachine.exception.MachineCoffeeException;
import com.coffeemachine.pojos.*;
import org.junit.*;
import org.junit.rules.ExpectedException;
import org.mockito.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;

public class ReportServiceTest {

    private static IReportService reportService;

    @InjectMocks
    private static OrderDrinkMakerServiceImpl orderDrinkMakerService;

    private static SoldServiceImpl soldService;

    @Mock
    private IGaneratorDrinkMakerProtocol generatorDrinkMakerProtocol;

    @Mock
    private BeverageQuantityChecker beverageQuantityChecker;

    @Mock
    private EmailNotifier emailNotifier;

    @Captor
    private ArgumentCaptor<Drink> drinkCaptor;

    @Captor
    private ArgumentCaptor<String> stringCaptor;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @BeforeClass
    public  static void setUpGeneratorDrinkMaker() {
        soldService = new SoldServiceImpl();
        reportService = new ReportServiceImpl(soldService);
        IGaneratorDrinkMakerProtocol ganeratorDrinkMakerProtocol = new GeneratorDrinkMakerProtocolImpl();
        orderDrinkMakerService = new OrderDrinkMakerServiceImpl( ganeratorDrinkMakerProtocol,soldService);

    }


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @After
    public void cleanUp() {
        soldService.creanUp();
    }

    @Test
    public void shouldPrintMessageWithNonDrinkSoldWhenGetReportWithoutDrinkSolds() {
        //Given
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        //When
        reportService.getReport();

        //Then
        assertTrue(outContent.toString().contains("Not Drink Sold"));

    }

    @Test
    public void testPrintReportWithSoldOneChocolate() throws MachineCoffeeException {
        //Given
        Order order = new Order();
        Drink drink = new Drink(0, TypeDrink.CHOCOLATE);
        order.setDrink(drink);
        order.setPrice(0.6);
        String typeDrinkKabel = drink.getType().getLabel();
        String protocol = typeDrinkKabel +": : ";
        when(generatorDrinkMakerProtocol.generateOrderDrinkProtocol(any(Drink.class))).thenReturn(protocol);
        when(beverageQuantityChecker.isEmpty(anyString())).thenReturn(false);
        doNothing().when(emailNotifier).notifyMissingDrink(anyString());

        orderDrinkMakerService.orderDrink(order);
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        //When
        List<SoldReport> report = reportService.getReport();

        //Then
        assertTrue(outContent.toString().contains("drink => CHOCOLATE || number => 1 || amount => 0.5 euro"));
        assertTrue(outContent.toString().contains("TOTAL: 0.5 euros"));

         verify(generatorDrinkMakerProtocol,never()).generateOrderMessageProtocol(stringCaptor.capture());
         verify(generatorDrinkMakerProtocol).generateOrderDrinkProtocol(drinkCaptor.capture());

    }


    @Test
    public void testPrintReportWithSoldTwoChocolate() throws MachineCoffeeException {
        //Given
        Order order = new Order();
        Drink drink = new Drink(0, TypeDrink.CHOCOLATE);
        order.setDrink(drink);
        order.setPrice(0.6);

        Order order2 = new Order();
        Drink drink2 = new Drink(1, TypeDrink.CHOCOLATE);
        order2.setDrink(drink2);
        order2.setPrice(0.5);

        String typeDrinkKabel = drink.getType().getLabel();
        String protocol = typeDrinkKabel +": : ";
        when(generatorDrinkMakerProtocol.generateOrderDrinkProtocol(any(Drink.class))).thenReturn(protocol);
        when(beverageQuantityChecker.isEmpty(anyString())).thenReturn(false);
        doNothing().when(emailNotifier).notifyMissingDrink(anyString());

        orderDrinkMakerService.orderDrink(order);
        orderDrinkMakerService.orderDrink(order2);

         ByteArrayOutputStream outContent = new ByteArrayOutputStream();
         System.setOut(new PrintStream(outContent));

        //When
        reportService.getReport();

        //Then
       assertTrue(outContent.toString().contains("drink => CHOCOLATE || number => 2 || amount => 1.0 euro"));
       assertTrue(outContent.toString().contains("TOTAL: 1.0 euros"));

       verify(generatorDrinkMakerProtocol,never()).generateOrderMessageProtocol(stringCaptor.capture());
       verify(generatorDrinkMakerProtocol,Mockito.times(2)).generateOrderDrinkProtocol(drinkCaptor.capture());

    }


    @Test
    public void testPrintReportWithSoldTwoChocolateAndOneCoffee() throws MachineCoffeeException {
        //Given
        Order orderChocolate1 = new Order();
        Drink chocolate1 = new Drink(0, TypeDrink.CHOCOLATE);
        orderChocolate1.setDrink(chocolate1);
        orderChocolate1.setPrice(0.6);

        Order orderChocolate2 = new Order();
        Drink chocolate2 = new Drink(1, TypeDrink.CHOCOLATE);
        orderChocolate2.setDrink(chocolate2);
        orderChocolate2.setPrice(0.5);

        Order orderCoffee = new Order();
        Drink coffee = new Drink(1, TypeDrink.COFFEE);
        orderCoffee.setDrink(coffee);
        orderCoffee.setPrice(0.6);


        String protocol = "Drink" +": : ";
        when(generatorDrinkMakerProtocol.generateOrderDrinkProtocol(any(Drink.class))).thenReturn(protocol);
        when(beverageQuantityChecker.isEmpty(anyString())).thenReturn(false);
        doNothing().when(emailNotifier).notifyMissingDrink(anyString());

        orderDrinkMakerService.orderDrink(orderChocolate1);
        orderDrinkMakerService.orderDrink(orderChocolate2);
        orderDrinkMakerService.orderDrink(orderCoffee);


        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        //When
        reportService.getReport();

        //Then
        assertTrue(outContent.toString().contains("drink => CHOCOLATE || number => 2 || amount => 1.0 euro"));
        assertTrue(outContent.toString().contains("drink => COFFEE || number => 1 || amount => 0.6 euro"));

        assertTrue(outContent.toString().contains("TOTAL: 1.6 euros"));

        verify(generatorDrinkMakerProtocol,never()).generateOrderMessageProtocol(stringCaptor.capture());
        verify(generatorDrinkMakerProtocol,Mockito.times(3)).generateOrderDrinkProtocol(drinkCaptor.capture());

    }

}
