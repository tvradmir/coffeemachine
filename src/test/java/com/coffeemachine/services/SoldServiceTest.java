package com.coffeemachine.services;

import com.coffeemachine.pojos.SoldEntity;
import com.coffeemachine.pojos.TypeDrink;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class SoldServiceTest {

   private static ISoldService soldService;


    @BeforeClass
    public  static void setUpGeneratorDrinkMaker() {
        soldService = new SoldServiceImpl();
    }

    @Test
    public void shouldReturnSoldEntityWithIdNotZeroWhenPersistNewReportEntity(){
        //Given
        SoldEntity soldEntity = new SoldEntity(TypeDrink.CHOCOLATE.name(), 0.5, 1.0);
        //When
        soldService.save(soldEntity);
        //Then
        System.out.println(soldEntity.getId());

        Assert.assertTrue( soldEntity.getId()!=0);

    }


    @Test
    public void shouldReturnNonEmptyListOfSoldEntityWithIdNotZeroWhenGetAllGivenCreatedListOfSolds(){
        //Given

        List<SoldEntity> soldEntityList = new ArrayList<>();
        soldEntityList.add(new SoldEntity(TypeDrink.CHOCOLATE.name(), 0.6, 1.0));
        soldEntityList.add(new SoldEntity(TypeDrink.CHOCOLATE.name(), 0.6, 0.6));
        soldEntityList.add(new SoldEntity(TypeDrink.COFFEE.name(), 0.5, 1.0));
        soldEntityList.add(new SoldEntity(TypeDrink.TEA.name(), 0.4, 0.4));
        soldService.saveAll(soldEntityList);



        //When
        List<SoldEntity> allSoldEntities = soldService.getAll();
        //Then
        Assert.assertFalse( allSoldEntities.isEmpty());


    }
}
